<?php
include "Databaseconnect.php";
include "init.php";
?>
    <link rel="stylesheet" href="css/chatbox.css">
<?php
    $sql = "SELECT * FROM messages ORDER BY `ID` DESC  limit 100";
    $stmt = $database->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();

?>

<div id="shouts">
    <div style="padding-top:50px;" class="alles">
    <?php
    $counter = 0;
    foreach ($result as $row) {
    ?>
        <div class="container <?php if($_SESSION['username'] == $row['U_Name']) echo "You";  else echo "Other";?>">
            <div class="user<?php if($_SESSION['username'] == $row['U_Name']) echo "Right";?>">
                <p class="txtName"><?php echo $row['U_Name']; ?></p>
                <p class="txtBeschrijving"><?php echo $row['Message']; ?></p>
                <p class="time-<?php if($_SESSION['username'] == $row['U_Name'])echo "left"; else echo "right"; ?>"><?php echo $row['Timestamp']; ?></p>
            </div>
        </div>
        <?php $counter++;
    } ?>
    </div>
</div>

