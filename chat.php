<?php
include "Databaseconnect.php";
include "init.php";
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>eCare Chat</title>

    <link rel="stylesheet" href="css/chatbox.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>



</head>


<body>
<div class="BackClass">
    <button class="backbutton" style="margin-top:6px;" class="btn btn-primary btn-sm btnBack" > <i id="fontAwesome" class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></button>
    <center> <strong class="chattekst"> eCare - Chat voor de zorg </strong> </center>
</div>


<div class="input" id="inputs">
    <form>
        <textarea placeholder="Typ hier uw bericht.." class="btn-input form-control input-sm chat_input" id="shout" rows="5"></textarea>
        <input type="submit" class="btn-chat btn btn-primary btn-sm SendSubmit" id="SendSubmit" value="Versturen">
    </form>
</div>

<div id="shoutbox" class="shoutbox" style="max-height:60%; overflow-y: auto;"></div>
</body>
</html>

<script>
    $(document).on('click', '.backbutton', function() {
        window.location = "landingpage.php";
    })
    $(document).ready(function () {

        $('#SendSubmit').on('click', function () {
            loadMessages();



            var name = $('#name').val();
            var shout = $('#shout').val();
            var dataString = 'name=' + name + '&shout=' + shout;

            if (shout == '') {
                alert('Please fill in a message.');
            } else {
                $.ajax({
                    type: "POST",
                    url: "insertchatDB.php",
                    data: dataString,
                    cache: false,
                    success: function (html) {

                    }
                });
            }
            document.getElementById('shout').value = '';
            return false;
        });


        function loadMessages() {
            $(".shoutbox").load("messages.php");
            return false;
        }

        loadMessages();
        setInterval(loadMessages, 2000);


        return false;
    });

</script>