<?php
if (isset($_POST['submit'])) {
    if ($_SESSION["hasPin"] == true) {
        $pincode = $_POST["pincode-input1"];
        $result = $database->prepare("SELECT * FROM user WHERE U_Name=? and C_Code=?");
        $result->bindParam(1, $_SESSION["username"]);
        $result->bindParam(2, $pincode);
        $result->execute();
        $countRows = $result->rowCount();

        if ($countRows == 1) {
            $_SESSION['LoggedIn'] = false;
            $_SESSION['pincode'] = $pincode;
            header("location: landingpage.php");
        }
    } else {
        $pincode = $_POST["pincode-input2"];
        if (strlen($pincode) == 4 && $pincode == $_POST["pincode-input1"]) {
            $stmt = $database->prepare("UPDATE user SET C_Code = :Pincode WHERE U_Name= :Username");
            $stmt->bindParam(':Pincode', $pincode);
            $stmt->bindParam(':Username', $_SESSION['username']);

            if ($stmt->execute()) {
                $_SESSION['LoggedIn'] = false;
                $_SESSION["pinCreated"] = true;
                $_SESSION['hasPin'] = true;
                header("location: pinLog.php");
            }
        }
    }
}
