<?php
include "Databaseconnect.php";
include "init.php";
if(isset($_SESSION['pinActivated'])) {
    if ($_SESSION['pinActivated'])header("location: landingpage.php");
}
?>
<html>
	<head>
        <meta charset="UTF-8">
        <script src="js/index.js"></script>
        <script src="js/prefixfree.min.js"></script>
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-pincode-input.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
    	  <script type="text/javascript" src="js/bootstrap-pincode-input.js"></script>
				<link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/normalize.css">

        <title>eCare - PIN</title>
    </head>
	<body>


<div class="toplogo">
		<img src="img/logo.png" alt="logo" id="Logo">
</div>


<div class="page_name">
    <center><h3 style="" id="inloggentext">Welkom <?php echo $_SESSION['username']; ?></h3></center>
</div>

<form method="post">
    <div class="pagina" style="border:2px solid white; margin-left: 35%">
        <div class="pincode">
          <p class="pintekst" style="margin-left: 5%">Vul Pincode in:</p>
        <input type="password" id="pincode-input1" name="pincode-input1"> <br>
      </div>
    </div>
    <input style="position:absolute; width:120px; left:35%; right:150%;" id="buttonLogin" class="button" type="submit" name="submit" value="Log In">
</form>

<?php include "PinAuthenticatie.php"; ?>


<center><h6 style="margin-top:10%" href="" id="inloggentext2">Bent u dit niet?</h6></center>
<br>
<center><a href="destroy.php">Andere Gebruiker</a></center>
<script>
    $(document).ready(function() {
        $('#pincode-input1').pincodeInput({hidedigits:true,complete:function(value, e, errorElement){

        }});

        $('#pincode-input2').pincodeInput({hidedigits:true,complete:function(value, e, errorElement){

        }});

        $('#pincode-input5').pincodeInput({hidedigits:true,inputs:4,placeholders:"0 0 0 1",change: function(input,value,inputnumber){
          $("#pincode-callback2").html("onchange from input number "+inputnumber+", current value: " + value);
        }});
        $('#pincode-input4').pincodeInput({hidedigits:true,inputs:4});
        $('#pincode-input3').pincodeInput({hidedigits:true,inputs:5});
        $('#pincode-input2').pincodeInput({hidedigits:true,inputs:6,complete:function(value, e, errorElement){
          $("#pincode-callback").html("Complete callback from 6-digit test: Current value: " + value);
        }});
    });
</script>

   </body>
</html>
