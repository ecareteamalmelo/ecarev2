<?php
include "Databaseconnect.php";
include "init.php";
if(isset($_SESSION['LoggedIn'])) {
    if ($_SESSION['LoggedIn'] == false) {
        $_SESSION['LoggedIn'] = true;
    }
}
if ($_SESSION['pinCreated'])header("location: pinLog.php");
?>
<html>
	<head>
        <meta charset="UTF-8">
        <script src="js/index.js"></script>
        <script src="js/prefixfree.min.js"></script>
				<meta name="viewport" content="width=device-width">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-pincode-input.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
    	  <script type="text/javascript" src="js/bootstrap-pincode-input.js"></script>
				<link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/normalize.css">

        <title>eCare - PIN</title>
    </head>
	<body>


<div class="toplogo">
		<img src="img/logo.png" alt="logo" id="Logo">
</div>


<div class="page_name">
		<h3 style="margin-left:10%;" id="inloggentext">PIN</h3>
    <p style="margin-left:10%;" class="cijfertekst">Voer hier 4 cijferige pincode om te voorkomen dat andere gebruikers toegang krijgen tot uw account. Onthoud daarom uw code goed!</p>
</div>

<form method="post">
  <div class="pagina" style="border:2px solid white; margin-top:5%; margin-left: 35%">
      <div class="pincode">
        <p class="pintekst"> Nieuwe Pincode:</p>
        <input type="password" id="pincode-input2" name="pincode-input2">
        <p style="" class="pintekst">Herhaal Pincode:</p>
      <input type="password" id="pincode-input1" name="pincode-input1"> <br>
          <input style="position:absolute; width:120px; left:35%; right:150%;" id="buttonLogin" class="button" type="submit" name="submit" value="Log In">

      </div>
  </div>
</form>
<?php include "PinAuthenticatie.php"; ?>

<script>
    $(document).ready(function() {
        var pincodeA = $("#pincode-input1").val();
        var pincodeB = $("#pincode-input2").val()
        $('#pincode-input1').pincodeInput({hidedigits:true,complete:function(value, e, errorElement){

        }});

        $('#pincode-input2').pincodeInput({hidedigits:true,complete:function(value, e, errorElement){

        }});

        $('#pincode-input5').pincodeInput({hidedigits:true,inputs:4,placeholders:"0 0 0 1",change: function(input,value,inputnumber){
          $("#pincode-callback2").html("onchange from input number "+inputnumber+", current value: " + value);
        }});
        $('#pincode-input4').pincodeInput({hidedigits:true,inputs:4});
        $('#pincode-input3').pincodeInput({hidedigits:true,inputs:5});
        $('#pincode-input2').pincodeInput({hidedigits:true,inputs:6,complete:function(value, e, errorElement){
          $("#pincode-callback").html("Complete callback from 6-digit test: Current value: " + value);
        }});
    });
</script>

   </body>
</html>
