<?php

// Als je weer terug in de index komt met een pin, moet je naar de pinLog gestuurd worden.
// Je raakt de session pas kwijt na het uitloggen of het 5 minuten inactief zijn.
if(isset($_SESSION['hasPin'])) {
    if ($_SESSION['hasPin'] == true) {
        header("location: pinLog.php");
    }
}

// Om te voorkomen dat iemand in Oudpin.php komt met een pin moet er gecheckt worden of $_SESSION['hasPin'] bestaat
// Het probleem hiermee is, is dat $_SESSION['hasPin'] al in gebruik is. We kunnen dit fixen door gewoon de button
// een andere naam te geven en daarom de POST opvragen, zodat we 2 verschillende buttons hebben. Voorkomt ook verwarring.

// Bij de chat op het moment dat je een tweede bericht stuurt, zou het misschien wel handig zijn om de naam er niet meer
// neer te zetten, zoals bij whatsapp. Ook een naam kleurtje zou leuk zijn, zodat je elkaar makkelijk uit elkaar kan houden

if (isset($_POST['submit'])) {
    $result = $database->prepare("SELECT * FROM user WHERE U_Name=? and U_Password=?");
    $result->bindParam(1, $_POST['myUsername']);
    $result->bindParam(2, $_POST['myPassword']);
    $result->execute();
    $countRows = $result->rowCount();

    if ($countRows == 1) {
        $stmt = $database->prepare('SELECT C_Code FROM user WHERE U_Name=?');
        $stmt->bindParam(1, $_POST['myUsername'], PDO::PARAM_STR);
        $stmt->execute();
        $results = $stmt->fetch();
        if ($results ["C_Code"] == 0) {
            $_SESSION['LoggedIn'] = false;
            $_SESSION['pinActivated'] = false;
            $_SESSION["pinCreated"] = false;
            $_SESSION["hasPin"] = false;
            $_SESSION["username"] = $_POST['myUsername'];
            header("location: pin.php");
        } else {
            $_SESSION['LoggedIn'] = false;
            $_SESSION['pinActivated'] = false;
            $_SESSION["pinCreated"] = false;
            $_SESSION["username"] = $_POST['myUsername'];
            $_SESSION["hasPin"] = true;
            header("location: pinLog.php");
        }
    } else {
        $_SESSION['bestaatniet'] = 'true';
    }

}