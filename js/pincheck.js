    $(document).ready(function() {
        $('#pincode-input1').pincodeInput({hidedigits:false,complete:function(value, e, errorElement){

          $("#pincode-callback").html("This is the 'complete' callback firing. Current value: " + value);
          if(value!="1234"){
            $(errorElement).html("De code is niet juist!");
          }else{
            alert('De code is juist!');
          }
        }});

        $('#pincode-input2').pincodeInput({hidedigits:false,complete:function(value, e, errorElement){

          $("#pincode-callback").html("This is the 'complete' callback firing. Current value: " + value);
          if(value!="1234"){
            $(errorElement).html("De code is niet juist!");
          }else{
            alert('De code is juist!');
          }
        }});

        $('#pincode-input5').pincodeInput({hidedigits:true,inputs:4,placeholders:"0 0 0 1",change: function(input,value,inputnumber){
          $("#pincode-callback2").html("onchange from input number "+inputnumber+", current value: " + value);
        }});
        $('#pincode-input4').pincodeInput({hidedigits:false,inputs:4});
        $('#pincode-input3').pincodeInput({hidedigits:false,inputs:5});
        $('#pincode-input2').pincodeInput({hidedigits:false,inputs:6,complete:function(value, e, errorElement){
          $("#pincode-callback").html("Complete callback from 6-digit test: Current value: " + value);
        }});
    });
