<?php
include "Databaseconnect.php";
include "init.php";
if(isset($_SESSION['LoggedIn'])) {
    if ($_SESSION['LoggedIn'] == true) {
        header("location: pin.php");
    }
}
?>

<html>
	<head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/style.css">
<!--        ish erro ding-->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!--        error einde-->
        <link rel="stylesheet" href="css/normalize.css">
        <script src="js/index.js"></script>
        <script src="js/prefixfree.min.js"></script>
				<meta name="viewport" content="width=device-width">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>eCare - Login</title>
    </head>
	<body>


        <div class="toplogo">
                <img src="img/logo.png" alt="logo" id="Logo">
        </div>


        <div class="page_name">
                <h3 id="inloggentext">INLOGGEN</h3>
        </div>


        <div class="login">
            <form method="post">
                        <br><br><br><br>
                        <i class="fa fa-user" aria-hidden="true"></i> Gebruikersnaam
                    <div>
                        <input type="text" name="myUsername" placeholder="" required="required" />
                    </div>
                <hr>
                        <i class="fa fa-lock" aria-hidden="true"></i> Wachtwoord
                <input type="password" name="myPassword" placeholder="" required="required" />
                <br><br>
                    <div>
<!--                        <div id="alert_placeholder">-->
<!--                            <div class="alert alert-success alert-dismissable" id="myAlertHide">-->
<!--                                <a href="#" class="close">&times;</a>-->
<!--                                <strong>R</strong>EEEEEEEEE-->
<!--                            </div>-->
<!--                        </div>-->
                        <input id="buttonLogin" class="button" type="submit" name="submit" id="submit" value="LogIn">
                    </div>
            </form>
        </div>
        <?php include "InlogAuthenticatie.php"; ?>
    </body>
<!--  not working error messages. need to use ajax calls to get this to work  -->
    <script>
//<!--        var name="-->//";
//        console.log(name);
//        $( document ).ready(function() {
//            $("#alert_placeholder").hide();
//        });
//
//        $("#myAlertHide").click(function(){
//            $("#alert_placeholder").hide();
//        });
//
//        //if statement
//        if('true' != name) {
//            $("#alert_placeholder").show();
//            console.log('komt drin');
//        }
//


     </script>
</html>
