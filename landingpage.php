<?php
include "Databaseconnect.php";
session_start();
if(isset($_SESSION['pinActivated'])) {
    if ($_SESSION['pinActivated'] == false) {
        $_SESSION['pinActivated'] = true;
    }
}
?>
<html>
	<head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/normalize.css">
        <script src="js/index.js"></script>
        <script src="js/prefixfree.min.js"></script>
				<meta name="viewport" content="width=device-width">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>eCare - Landingpage</title>
    </head>
	<body>


<div class="toplogo">
		<img src="img/logoWhite.png" alt="logo" id="Logo">
</div>


<div class="page_name">
    <center><h3 style="margin-top:90px" id="inloggentext">Welkom <?php echo $_SESSION['username']; ?> </h3></center>
</div>



<div style="    margin-left: 23px;" class="landingpage">
	 <button class="landingbutton"><i class="fa fa-calendar fa-3x" aria-hidden="true"></i><br/> Dagplanning</i></button>

	 <button class="landingbutton"><i class="fa fa-map fa-3x" aria-hidden="true"></i><br/>  Routeplanner</i></button>

	 <button class="landingbutton"><i class="fa fa-file-text fa-3x" aria-hidden="true"></i><br/>  Patientgegevens</i></button>

	 <button class="landingbutton " id="people"><i class="fa fa-user-md fa-3x" aria-hidden="true"></i><br/> Eerdere bezoeken</i></button>
	 <button  id="chatbutton" class="landingbutton"><i class="fa fa-comments-o fa-3x" aria-hidden="true"></i><br/> Chat</i></button>
</div>

<br>

<center><a style="" href="destroy.php">Log Out</a></h5></center><br><br>


<script type="text/javascript">
    document.getElementById("chatbutton").onclick = function () {
        location.href = "chat.php";
    };
</script>



<style>
body, html{
	overflow: scroll;
}

.toplogo{
	position: relative;
}
</style>



</body>
</html>
