<?php
include "init.php";
session_destroy();
unset($_SESSION['username']);
unset($_SESSION['hasPin']);
unset($_SESSION['pinCreated']);
unset($_SESSION['LoggedIn']);
header("location: index.php");
die;